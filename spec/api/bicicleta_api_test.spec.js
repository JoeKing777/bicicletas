const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const server = require('../../bin/www'); // abre el server y lo cierra al fin del test

const base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () => {
    beforeEach(done => {
        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', () => {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(done => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET bicicletas/', () => {
        it("Status 200", done => {
            request.get(base_url, (err, response, body) => {
                let result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST bicicletas/create', () => {
        it('Status 201', done => {
            let headers = { 'content-type': 'application/json' };
            let aBici = { "code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 };
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                let bici = JSON.parse(body).bicicletal
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });

});