var mongoose = require('mongoose');
var bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, {
            useUnifiedTopology: true,
            useNewUrlParser: true
        });
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('Connected to database');
            done();
        })
    });
    afterEach(function(done) {
        bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('bicicleta.createInstance', () => {
        it('Crea una instancia de bicicleta', () => {
            var bici = bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicletas.allBicis', () => {
        it('comienza vacía', (done) => {
            bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('bicicleta.add', () => {
        it('Agrega solo una bici', (done) => {
            var aBici = new bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            bicicleta.allBicis(function(err, newBici) {
                if (err) console.log(err);
                bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });
    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);
                    var aBici2 = new bicicleta({ code: 2, color: "roja", modelo: "urbana" });
                    bicicleta.add(aBici2, function(err, newBici) {
                        if (err) console.log(err);
                        bicicleta.findByCode(1, function(error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                })

            });
        });


    });
});

/*
beforeEach(() => {
    bicicleta.allBicis = [];

});
describe("bicicleta.allBicis", () => {
    it('Comienza vacía', () => {
        expect(bicicleta.allBicis.length).toBe(0);
    });
});
describe("bicicleta.add", () => {
    it('Agregamos una', () => {
        expect(bicicleta.allBicis.length).toBe(0); //estado inicial
        var a = new bicicleta(1, 'rojo', 'urbana', [51.505, -0.05]);
        bicicleta.add(a);
        expect(bicicleta.allBicis.length).toBe(1); // después de agregar
        expect(bicicleta.allBicis[0]).toBe(a);
    });
});
describe("bicicleta.findById", () => {
    it('Buscar la ID digitada (1)', () => {
        expect(bicicleta.allBicis.length).toBe(0); //estado inicial

        var aBici1 = new bicicleta(1, 'morada', 'urbana', [51.505, -0.15]);
        var aBici2 = new bicicleta(2, 'morada', 'urbana', [51.505, -0.18]);
        bicicleta.add(aBici1);
        bicicleta.add(aBici2);
        var targetBici = bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color); // después de agregar
        expect(targetBici.modelo).toBe(aBici1.modelo);
    });
});
*/